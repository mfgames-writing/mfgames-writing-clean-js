## [3.0.5](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/compare/v3.0.4...v3.0.5) (2018-08-12)


### Bug Fixes

* removing break-word CSS tags ([943c3ad](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/commit/943c3ad))

## [3.0.4](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/compare/v3.0.3...v3.0.4) (2018-08-12)


### Bug Fixes

* explict alignment and word break for p, i, em, b, and strong ([a4d7283](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/commit/a4d7283))

## [3.0.3](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/compare/v3.0.2...v3.0.3) (2018-08-09)


### Bug Fixes

* fixing issues with packages and paths ([636f504](https://gitlab.com/mfgames-writing/mfgames-writing-clean-js/commit/636f504))
